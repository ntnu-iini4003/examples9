#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

ostream &operator<<(ostream &out, const vector<int> &table) {
  for (auto &e : table)
    out << e << " ";
  return out;
}

int main() {
  vector<int> v1 = {3, 3, 12, 14, 17, 25, 30};
  cout << "v1: " << v1 << endl;

  vector<int> v2 = {2, 3, 12, 14, 24};
  cout << "v2: " << v2 << endl;

  // Binærsøk. Funksjonen binary_search() sier om et element
  // er i en vektor, men ikke hvor. Bruker lower_bound()
  // (ev. upper_bound()) til å fortelle oss det.
  if (binary_search(v1.begin(), v1.end(), 17)) {
    cout << "17 fins i v1\n";
  } else
    cout << "17 fins ikke i v1\n";

  // Verdien 17 er i v1, men på hvilken indeks?
  auto pos = lower_bound(v1.begin(), v1.end(), 17);
  cout << "17 er på indeks " << (pos - v1.begin()) << endl;

  // 25 er ikke i v2. Lager en kopi av v2, skal sette inn 25 der.
  auto v3 = v2;
  pos = lower_bound(v3.begin(), v3.end(), 25);
  cout << "25 skal inn på posisjon " << (pos - v3.begin()) << endl;
  v3.insert(pos, 25);
  cout << "V3 er lik v2 med 25 satt inn: " << v3 << endl;

  // Includes
  if (includes(v1.begin(), v1.end(), v2.begin() + 1, v2.begin() + 3))
    cout << "Er med!\n";
  else
    cout << "Er ikke med!\n";

  // Fletting
  vector<int> result;
  result.resize(v1.size() + v2.size()); // eksakt riktig størrelse
  merge(v1.begin(), v1.end(), v2.begin(), v2.end(), result.begin());
  cout << "V1 og V2 flettet: " << result << endl;

  // Fjerner dubletter i result
  auto end = unique(result.begin(), result.end());
  result.erase(end, result.end()); // reduserer størrelsen
  cout << "V1 og V2 flettet, uten dubletter: " << result << endl;

  // Mengdeoperasjoner
  result.resize(v1.size() + v2.size()); // minst stort nok
  end = set_union(v1.begin(), v1.end(), v2.begin(), v2.end(), result.begin());
  result.erase(end, result.end()); // reduserer størrelsen
  cout << "V1 union V2: " << result << endl;

  result.resize(v1.size() + v2.size()); // minst stort nok
  end = set_intersection(v1.begin(), v1.end(), v2.begin(), v2.end(), result.begin());
  result.erase(end, result.end());
  cout << "V1 snitt V2: " << result << endl;
}

/* Kjøring av programmet:
v1: 3 3 12 14 17 25 30 
v2: 2 3 12 14 24 
17 fins i v1
17 er på indeks 4
25 skal inn på posisjon 5
V3 er lik v2 med 25 satt inn: 2 3 12 14 24 25 
Er med!
V1 og V2 flettet: 2 3 3 3 12 12 14 14 17 24 25 30 
V1 og V2 flettet, uten dubletter: 2 3 12 14 17 24 25 30 
V1 union V2: 2 3 3 12 14 17 24 25 30 
V1 snitt V2: 3 12 14
*/
