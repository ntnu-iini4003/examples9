#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

ostream &operator<<(ostream &out, const vector<int> &table) {
  for (auto &e : table)
    out << e << " ";
  return out;
}

int main() {
  vector<int> v1 = {3, 3, 12, 14, 17, 25, 30};
  cout << "v1: " << v1 << endl;
  vector<int> v2 = {2, 3, 12, 14, 24};
  cout << "v2: " << v2 << endl;

  swap(v1, v2); // bytter om v1 og v2
  cout << "Etter ombytting:" << endl;
  cout << "v1: " << v1 << endl;
  cout << "v2: " << v2 << endl;

  auto old_size = v1.size();
  v1.resize(v1.size() + v2.size());
  copy(v2.begin(), v2.end(), v1.begin() + old_size);
  cout << "Nå er v1 lik v1 + v2: " << v1 << endl;

  // Bytter om elementene [0, 5> og [5, 10>
  swap_ranges(v1.begin(), v1.begin() + 5, v1.begin() + 5);
  cout << "V1, byttet om elementene [0, 5> og [5, 10>: " << v1 << endl;

  // Legger inn 3 i de fem første elementene
  fill(v1.begin(), v1.begin() + 5, 3);
  cout << "V1 med tallet 3 i element [0, 5>: " << v1 << endl;

  // Lager en ny vektor der alle 3-tall er byttet ut med 300
  vector<int> copy;
  copy.resize(v1.size());
  replace_copy(v1.begin(), v1.end(), copy.begin(), 3, 300);
  cout << "Kopi: " << copy << endl;

  // Snur v1
  vector<int> v1_reversed(v1.size());
  reverse_copy(v1.begin(), v1.end(), v1_reversed.begin());
  cout << "V1, snudd: " << v1_reversed << endl;

  // Roterer v1 3 posisjoner
  cout << "V1, før rotasjonen: " << v1 << endl;
  rotate(v1.begin(), v1.begin() + 3, v1.end());
  cout << "V1, rotert 3 posisjoner: " << v1 << endl;

  // Lager en vektor med størrelse 3, finner deretter alle permutasjoner
  vector<int> table;
  table.emplace_back(1);
  table.emplace_back(2);
  table.emplace_back(3);
  // Antall permutasjoner er lik 3! = 1*2*3 = 6.
  // Lager sju permutasjoner, da skal perm. 7 være lik perm. 1
  for (size_t i = 0; i < 7; ++i) {
    next_permutation(table.begin(), table.end());
    cout << "Permutasjon " << (i + 1) << ": ";
    cout << table << endl;
  }
}

/* Kjøring av programmet:
v1: 3 3 12 14 17 25 30 
v2: 2 3 12 14 24 
Etter ombytting:
v1: 2 3 12 14 24 
v2: 3 3 12 14 17 25 30 
Nå er v1 lik v1 + v2: 2 3 12 14 24 3 3 12 14 17 25 30 
V1, byttet om elementene [0, 5> og [5, 10>: 3 3 12 14 17 2 3 12 14 24 25 30 
V1 med tallet 3 i element [0, 5>: 3 3 3 3 3 2 3 12 14 24 25 30 
Kopi: 300 300 300 300 300 2 300 12 14 24 25 30 
V1, snudd: 30 25 24 14 12 3 2 3 3 3 3 3 
V1, før rotasjonen: 3 3 3 3 3 2 3 12 14 24 25 30 
V1, rotert 3 posisjoner: 3 3 2 3 12 14 24 25 30 3 3 3 
Permutasjon 1: 1 3 2 
Permutasjon 2: 2 1 3 
Permutasjon 3: 2 3 1 
Permutasjon 4: 3 1 2 
Permutasjon 5: 3 2 1 
Permutasjon 6: 1 2 3 
Permutasjon 7: 1 3 2
*/
